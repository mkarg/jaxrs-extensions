# Common Extensions To JAX-RS

This library provides common extensions to the JAX-RS standard, like exception mappers for common exception classes, etc.

# CI/CD Status

[![pipeline status](https://gitlab.com/quipsy/jaxrs-extensions/badges/master/pipeline.svg)](https://gitlab.com/quipsy/jaxrs-extensions/commits/master)

